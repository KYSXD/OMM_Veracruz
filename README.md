OMM Veracruz

Repositorio de algunos materiales de la olimpiada

Las carpetas actuales (24 junio 2015) estan distribuidas de la siguiente manera:

* EntrenamientoKYE
-Algunas notas del entrenamiento Kevin-Eunice 2014
* Est_Term
-Estales varios, terminados
* EstatalesVer
-Estales, distribuidos en dos carpetas con los .tex de cada examen y los archivos en geogebra. Para crear pdf basta con compilar EstVer.tex
* FB_2014
-Problemas de Facebook 2014
* Geo_OMM2015
-Notas para el entrenamiento de geometría 2015 pre-primer selectivo
* Prioridades
-Prioridades y archivos que "deberia" terminar con urgencia